## FirstWeekTask

The name of the application is **Info App**. Where people can give answers to some particular questions and see the results.

### How to use this app

At first, open the directory "**firstweektask-develop**".

Run the following command to install python and django.
```
sudo apt install python3.8
python3 -m pip install Django
```

Write the following commands to run the app.

```
python3 manage.py migrate
python3 manage.py runserver
```
Open the link [http://127.0.0.1:8000/info
](http://127.0.0.1:8000/info
) in your browser to see the application.

You can go to [http://127.0.0.1:8000/admin
](http://127.0.0.1:8000/admin
) to open the admin site using the username as **admin** and password as **admin123456**.

Give answer of the questions and enjoy the app!
