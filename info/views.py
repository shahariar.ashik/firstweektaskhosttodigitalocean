from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.views import generic
from django.utils import timezone

from .models import Question, Answer


class IndexView(generic.ListView):
    template_name = "info/index.html"
    context_object_name = "latest_question_list"

    def get_queryset(self):
        """
        Return last five asked questions which are published(not including
        those which are set to be asked in the future).
        """
        return Question.objects.filter(
            pub_date__lte=timezone.now()
        ).order_by("-pub_date")[:5]


class DetailView(generic.DetailView):
    model = Question
    template_name = "info/detail.html"

    def get_queryset(self):
        """
        Excludes all questions that will be published in future
        """
        return Question.objects.filter(pub_date__lte=timezone.now())


class ResultsView(generic.DetailView):
    model = Question
    template_name = "info/results.html"


def answer_list(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    try:
        selected_answer = request.POST["question_answer"]
        if not selected_answer:
            raise KeyError("No input")
    except (KeyError, Answer.DoesNotExist):
        # Redisplay the question answer form
        return render(request, "info/detail.html", {
            "question": question,
            "error_message": "You didn't give the answer.",
        })
    else:
        selected_question = Question.objects.get(pk=question.id)
        selected_question.answer_set.create(answer_text=selected_answer)
        return HttpResponseRedirect(reverse('info:results', args=(question.id,)))


